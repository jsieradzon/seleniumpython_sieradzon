import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture()
def browser():
    # Faza 1 - coś co się wykona przed każdym testem
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://timvroom.com/selenium/playground/')

    # Faza 2 - coś co przekazujemy do każdego testu
    yield browser

    # Faza 3 - coś co się wykona po każdym teście
    browser.quit()

def test_waiting(browser):
    browser.find_element(By.LINK_TEXT, 'click then wait').click()
    wait = WebDriverWait(browser, 10)
    wait.until(EC.presence_of_element_located((By.LINK_TEXT, 'click after wait')))
    assert browser.find_element(By.LINK_TEXT, 'click after wait').is_displayed()


